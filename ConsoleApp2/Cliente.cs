﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class Cliente
    {
        //Propiedades
        // Si es un cliente activo o si es pasivo
        private String Tipo;

        public String Tipo
        {
            get { return this.tipo; }
            set { this.tipo = value; }
        }
        // Si es cuenta de ahorro, cliente 
        public String TipoCuenta
        {
            get { return TipoCuenta; }
            set { TipoCuenta = value; }
        }
        // Si es tipo deposito, cheque, transferencia, etc
        private String TipoTransaccion
        {
            get { return TipoTransaccion; }
            set { TipoTransaccion = value; }
        }
    }
}
